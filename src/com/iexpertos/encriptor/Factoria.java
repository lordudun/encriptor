package com.iexpertos.encriptor;

import java.lang.reflect.Constructor;
import java.security.InvalidParameterException;


public class Factoria {

	private static String paqueteReglas = "com.iexpertos.encriptor.regla.";
	
	public Encriptor obtenerEncriptador(String tipoDeEncriptador) {
		
		return obtenerEncriptador(tipoDeEncriptador, null);
	}
	
	public Encriptor obtenerEncriptador(String tipoDeEncriptador, String parametros) {
		
		Regla regla = crearNuevaRegla(tipoDeEncriptador, parametros);
		
		return new Encriptor(regla);
	}
	
	private Regla crearNuevaRegla(String tipoDeRegla, String parametros) {
		
		Regla nuevaRegla = null;
		Class[] argumentosDeLaRegla = new Class[0];
		Object[] parametrosDeLaRegla = new Object[0];
		
		try {	
			
			if (parametros != null) {
				
				argumentosDeLaRegla = new Class[1];
				argumentosDeLaRegla[0] = String.class;
				
				parametrosDeLaRegla = new String[1];
				parametrosDeLaRegla[0] = parametros;
			}
			
			nuevaRegla = crearInstancia(argumentosDeLaRegla, parametrosDeLaRegla, tipoDeRegla);
			
		} catch (Exception e) {
			
			throw new InvalidParameterException();
		}
			
		return nuevaRegla;
	}
	
	private Regla crearInstancia(Class[] argumentosDeLaRegla, Object[] parametrosDeLaRegla, String tipoDeRegla) throws Exception{
		
		Class claseDeLaRegla;
		Constructor<Regla> constructorDeLaRegla;
		Regla nuevaRegla;
		
		claseDeLaRegla = Class.forName(paqueteReglas + tipoDeRegla);
		constructorDeLaRegla = claseDeLaRegla.getConstructor(argumentosDeLaRegla);
		nuevaRegla = (Regla) constructorDeLaRegla.newInstance(parametrosDeLaRegla);
		
		return nuevaRegla;
		
	}
}
