package com.iexpertos.encriptor;

import com.iexpertos.encriptor.sentencia.SentenciaComun;
import com.iexpertos.encriptor.sentencia.SentenciaSinEspacios;

public class Main {

	private static String Desplaza_Dos_Letras = "desplazaletra.ReglaDesplazaDosLetras";
	private static String Convierte_A_Numero_Y_SumaDos = "anumero.ReglaConvierteANumeroYSumaDos";
	private static String Desplaza_Dos_Letras_Cuando = "desplazaletra.ReglaDesplazaDosLetrasCuando";
	
	public static void main(String[] args) {
		
		Encriptor encriptador;
		Factoria factoria = new Factoria();
		Sentencia sentenciaHola = new SentenciaSinEspacios("hola");
		Sentencia sentenciaHolaQueTalEstas = new SentenciaComun("hola, que tal estas?");
		
		encriptador = factoria.obtenerEncriptador(Desplaza_Dos_Letras);
		System.out.println("1:" + encriptador.encripta(sentenciaHola));
		
		encriptador = factoria.obtenerEncriptador(Convierte_A_Numero_Y_SumaDos);
		System.out.println("2:" + encriptador.encripta(sentenciaHola));
		
		encriptador = factoria.obtenerEncriptador(Desplaza_Dos_Letras_Cuando, "o");
		System.out.println("3:" + encriptador.encripta(sentenciaHola));
		
		encriptador = factoria.obtenerEncriptador(Desplaza_Dos_Letras);
		System.out.println("4:" + encriptador.encripta(sentenciaHolaQueTalEstas));
		sentenciaHolaQueTalEstas.imprimePalabras(System.out);
	}

}
