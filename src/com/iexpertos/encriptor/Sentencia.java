package com.iexpertos.encriptor;

import java.io.PrintStream;

public interface Sentencia {

	public String convierte(Regla reglaAUtilizar);
	public String[] obtenPalabras();
	public void imprimePalabras(PrintStream canalDeImpresion);
}
