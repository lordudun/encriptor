package com.iexpertos.encriptor.regla.anumero;

import com.iexpertos.encriptor.Regla;

public class ReglaConvierteANumeroYSumaDos implements Regla {
	 
	@Override
	public String aplicaPara(char estaLetra) {
		
		int numeroDesplagazo = estaLetra + 2;
		return String.valueOf(numeroDesplagazo);
	}
}
