package com.iexpertos.encriptor.regla.desplazaletra;

import com.iexpertos.encriptor.Regla;

public class ReglaDesplazaDosLetras implements Regla {

	@Override
	public String aplicaPara(char estaLetra) {
		
		char caracterDesplazado = desplazaDos(estaLetra);
		
		return convertirCaracterEnLetra(caracterDesplazado);
	}
	
	protected String convertirCaracterEnLetra(char caracterAConvertir) {
		
		return String.valueOf(caracterAConvertir);
	}

	protected char desplazaDos(char estaLetra) {
		
		return (char)(estaLetra + 2);
	}
}
