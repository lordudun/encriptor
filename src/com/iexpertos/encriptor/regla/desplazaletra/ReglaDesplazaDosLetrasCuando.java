package com.iexpertos.encriptor.regla.desplazaletra;

import java.util.HashSet;
import java.util.Set;


public class ReglaDesplazaDosLetrasCuando extends ReglaDesplazaDosLetras {

	private Set<String> letrasASubstituir;
	
	public ReglaDesplazaDosLetrasCuando(String letrasASubstituir) {
		
		String letraActual;
		this.letrasASubstituir = new HashSet<String>();
		
		for(char caracterActual : letrasASubstituir.toCharArray()) {
			
			letraActual = convertirCaracterEnLetra(caracterActual);
			
			this.letrasASubstituir.add(letraActual);
		}
	}
	
	@Override
	protected char desplazaDos(char estaLetra) {
		
		char caracterDesplazado = estaLetra;
		
		if (esUnaDeLasLetraASubstituir(estaLetra)) {
	
			caracterDesplazado = super.desplazaDos(estaLetra);		
		}
		
		return caracterDesplazado;
	}
	
	private boolean esUnaDeLasLetraASubstituir(char caracterAComprobar) {
		
		String letraAComprobar = convertirCaracterEnLetra(caracterAComprobar);
		
		return letrasASubstituir.contains(letraAComprobar);
	}
}
