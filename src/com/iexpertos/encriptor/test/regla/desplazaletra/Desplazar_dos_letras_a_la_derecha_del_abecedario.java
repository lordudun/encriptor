package com.iexpertos.encriptor.test.regla.desplazaletra;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.iexpertos.encriptor.Regla;
import com.iexpertos.encriptor.regla.desplazaletra.ReglaDesplazaDosLetras;

public class Desplazar_dos_letras_a_la_derecha_del_abecedario {

	Regla reglaDeDesplazarDosLetras;
	
	@Before
	public void preparar_datos_del_test() {
		
		reglaDeDesplazarDosLetras = new ReglaDesplazaDosLetras();
	}
	
	@Test
	public void a_pasa_a_ser_c() {
		
		assertEquals("c", reglaDeDesplazarDosLetras.aplicaPara('a'));
	}
	
	@Test
	public void z_pasa_a_ser_una_barra() {
		
		assertEquals("|", reglaDeDesplazarDosLetras.aplicaPara('z'));
	}
	
	@Test
	public void P_pasa_a_ser_R() {
		
		assertEquals("R", reglaDeDesplazarDosLetras.aplicaPara('P'));
	}
}
