package com.iexpertos.encriptor.test.regla.desplazaletra;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.iexpertos.encriptor.Regla;
import com.iexpertos.encriptor.regla.desplazaletra.ReglaDesplazaDosLetrasCuando;

public class Desplaza_dos_letras_a_la_derecha_del_abecedario_si_es_una_letra_a_substituir {

	private String cualquierListaDeLetrasASubstituir = "aP";
	
	private Regla reglaDesplazaDosLetrasCuando;
	
	
	@Before
	public void preparar_datos_del_test() {
		
		reglaDesplazaDosLetrasCuando = new ReglaDesplazaDosLetrasCuando(cualquierListaDeLetrasASubstituir);
	}
	
	@Test
	public void a_se_substituye_por_c() {
		
		assertEquals("c", reglaDesplazaDosLetrasCuando.aplicaPara('a'));
	}
	
	@Test
	public void z_no_se_substituye_z() {
		
		assertEquals("z", reglaDesplazaDosLetrasCuando.aplicaPara('z'));
	}
	
	@Test
	public void P_se_substituye_por_R() {
		
		assertEquals("R", reglaDesplazaDosLetrasCuando.aplicaPara('P'));
	}
}
