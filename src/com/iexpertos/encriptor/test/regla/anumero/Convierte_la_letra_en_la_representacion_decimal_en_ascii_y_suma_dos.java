package com.iexpertos.encriptor.test.regla.anumero;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.iexpertos.encriptor.Regla;
import com.iexpertos.encriptor.regla.anumero.ReglaConvierteANumeroYSumaDos;

public class Convierte_la_letra_en_la_representacion_decimal_en_ascii_y_suma_dos {
	
	Regla reglaConvierteANumeroSumaDos;
	
	@Before
	public void preparar_datos_del_test() {
		
		reglaConvierteANumeroSumaDos = new ReglaConvierteANumeroYSumaDos();
	}
	
	@Test
	public void a_se_convierte_en_99() {
		
		assertEquals("99", reglaConvierteANumeroSumaDos.aplicaPara('a'));
	}
	
	@Test
	public void z_se_convierte_en_124() {
		
		assertEquals("124", reglaConvierteANumeroSumaDos.aplicaPara('z'));
	}
	
	@Test
	public void P_se_convierte_en_82() {
		
		assertEquals("82", reglaConvierteANumeroSumaDos.aplicaPara('P'));
	}
}
