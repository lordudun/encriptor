package com.iexpertos.encriptor.test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;

import com.iexpertos.encriptor.Encriptor;
import com.iexpertos.encriptor.Regla;
import com.iexpertos.encriptor.Sentencia;

public class Se_encripta_la_sentencia {

	private String sentenciaCualquiera = "pimpam";
	
	Encriptor encriptor;
	Regla reglaCualquiera = mock(Regla.class);
	Sentencia mockedSentencia = mock(Sentencia.class);
	
	@Before
	public void preparar_datos_del_test() {
		
		encriptor = new Encriptor(reglaCualquiera);
	}
	
	@Test
	public void para_una_sentencia_cualquiera() {
		
		when(mockedSentencia.convierte(reglaCualquiera)).thenReturn(sentenciaCualquiera);
		assertEquals(sentenciaCualquiera, encriptor.encripta(mockedSentencia));
	}
}
