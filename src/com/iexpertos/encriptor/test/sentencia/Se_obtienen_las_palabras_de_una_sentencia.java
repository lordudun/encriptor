package com.iexpertos.encriptor.test.sentencia;

import static org.junit.Assert.assertArrayEquals;

import org.junit.Test;

import com.iexpertos.encriptor.Sentencia;
import com.iexpertos.encriptor.sentencia.SentenciaComun;

public class Se_obtienen_las_palabras_de_una_sentencia {

	Sentencia sentenciaComun;
	
	@Test
	public void la_sentencia_esta_compuesta_de_varias_palabras() {
		
		sentenciaComun = new SentenciaComun("hola como estas");
		
		String[] letrasEsperadas = {"hola","como","estas"};
		
		assertArrayEquals(letrasEsperadas, sentenciaComun.obtenPalabras());
	}
	
	@Test
	public void la_sentencia_solo_tiene_una_palabra() {
		
		sentenciaComun = new SentenciaComun("hola");
		
		String[] letrasEsperadas = {"hola"};
		
		assertArrayEquals(letrasEsperadas, sentenciaComun.obtenPalabras());
	}
	
	@Test
	public void la_sentencia_es_vacia() {
		
		sentenciaComun = new SentenciaComun("");
		
		String[] letrasEsperadas = {""};
		
		assertArrayEquals(letrasEsperadas, sentenciaComun.obtenPalabras());
		
	}
}
