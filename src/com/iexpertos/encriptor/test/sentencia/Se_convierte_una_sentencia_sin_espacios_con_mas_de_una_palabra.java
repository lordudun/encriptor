package com.iexpertos.encriptor.test.sentencia;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyChar;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.security.InvalidParameterException;

import org.junit.Test;

import com.iexpertos.encriptor.Regla;
import com.iexpertos.encriptor.Sentencia;
import com.iexpertos.encriptor.sentencia.SentenciaSinEspacios;

public class Se_convierte_una_sentencia_sin_espacios_con_mas_de_una_palabra {
	
	private String letraCualquiera = "k";
	
	Regla mockedRegla = mock(Regla.class);
	Sentencia sentenciaSinEspacios;
	
	@Test(expected=InvalidParameterException.class)
	public void no_es_una_sentencia_valida() {
		
		sentenciaSinEspacios = new SentenciaSinEspacios("pa pa");
		String sentenciaEsperada = "kkkkk";
		
		when(mockedRegla.aplicaPara(anyChar())).thenReturn(letraCualquiera);
		assertEquals(sentenciaEsperada, sentenciaSinEspacios.convierte(mockedRegla));
	}
}
