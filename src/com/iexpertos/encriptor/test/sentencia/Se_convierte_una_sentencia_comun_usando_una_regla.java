package com.iexpertos.encriptor.test.sentencia;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyChar;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;

import com.iexpertos.encriptor.Regla;
import com.iexpertos.encriptor.Sentencia;
import com.iexpertos.encriptor.sentencia.SentenciaComun;

public class Se_convierte_una_sentencia_comun_usando_una_regla {

	private String sentenciaCualquiera = "pa p";
	private String letraCualquiera = "z";
	
	Regla mockedRegla = mock(Regla.class);
	Sentencia sentenciaComun;
	
	@Before
	public void preparar_datos_del_test() {
		
		sentenciaComun = new SentenciaComun(sentenciaCualquiera);
	}
	
	@Test
	public void se_utiliza_la_regla_correctamente() {
		
		String sentenciaEsperada = substiturTodasLasLetrasPor(sentenciaCualquiera, letraCualquiera);
		
		when(mockedRegla.aplicaPara(anyChar())).thenReturn(letraCualquiera);
		assertEquals(sentenciaEsperada, sentenciaComun.convierte(mockedRegla));
	}
	
	private String substiturTodasLasLetrasPor(String sentenciaATratar, String letraQueSubsituye) {
		
		String sentenciaSubstituida = "";
		
		for (int letraActual = 0; letraActual < sentenciaATratar.length(); letraActual++) {
			
			sentenciaSubstituida = sentenciaSubstituida.concat(letraQueSubsituye);
		}
		
		return sentenciaSubstituida;
	}
}
