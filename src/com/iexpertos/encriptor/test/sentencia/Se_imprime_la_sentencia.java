package com.iexpertos.encriptor.test.sentencia;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import java.io.PrintStream;

import org.junit.Test;

import com.iexpertos.encriptor.Sentencia;
import com.iexpertos.encriptor.sentencia.SentenciaComun;

public class Se_imprime_la_sentencia {

	Sentencia sentenciaComun;
	PrintStream mockedOut = mock(PrintStream.class);
	
	@Test
	public void la_sentencia_esta_compuesta_de_varias_palabras() {
		
		sentenciaComun = new SentenciaComun("hola como estas");
		
		sentenciaComun.imprimePalabras(mockedOut);
		
		verify(mockedOut).print("<hola>");
		verify(mockedOut).print("<como>");
		verify(mockedOut).print("<estas>");
	}
	
	@Test
	public void la_sentencia_solo_tiene_una_palabra() {
		
		sentenciaComun = new SentenciaComun("hola");
		
		sentenciaComun.imprimePalabras(mockedOut);
		
		verify(mockedOut).print("<hola>");
	}
	
	@Test
	public void la_sentencia_es_vacia() {
		
		sentenciaComun = new SentenciaComun("");
		
		sentenciaComun.imprimePalabras(mockedOut);
		
		verify(mockedOut).print("<>");
	}
}
