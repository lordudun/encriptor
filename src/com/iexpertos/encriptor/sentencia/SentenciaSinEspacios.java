package com.iexpertos.encriptor.sentencia;

import java.security.InvalidParameterException;

import com.iexpertos.encriptor.Regla;

public class SentenciaSinEspacios extends SentenciaComun {

	public SentenciaSinEspacios(String sentencia) {
		
		super(sentencia);
	}
	
	@Override
	public String convierte(Regla reglaAUtilizar) {
		
		esUnaSentenciaSinEspacios();
		return super.convierte(reglaAUtilizar);
	}
	
	private void esUnaSentenciaSinEspacios() {
		
		if (this.sentencia.contains(" "))
			throw new InvalidParameterException();
	}
	
}
