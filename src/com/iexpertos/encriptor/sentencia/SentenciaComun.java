package com.iexpertos.encriptor.sentencia;

import java.io.PrintStream;

import com.iexpertos.encriptor.Regla;
import com.iexpertos.encriptor.Sentencia;

public class SentenciaComun implements Sentencia {

	protected String sentencia;
	
	public SentenciaComun(String sentencia) {
		
		this.sentencia = sentencia;
	}
	
	@Override
	public String convierte(Regla reglaAUtilizar) {
		
		char[] letrasDeLaSentencia = sentencia.toCharArray();
		return aplicaLetraALetra(reglaAUtilizar, letrasDeLaSentencia);
	}
	
	@Override
	public String[] obtenPalabras() {
		return sentencia.split(" ");
	}
	
	@Override
	public void imprimePalabras(PrintStream canalDeImpresion) {
		
		String[] words = obtenPalabras();
		
		for (String word : words)
		{
			canalDeImpresion.print("<" + word + ">");
		}
	}
	
	private String aplicaLetraALetra(Regla reglaAUtilizar, char[] letrasDeLaSentencia) {
		String sentenciaEncriptada = "";
		
		for (char letraActual : letrasDeLaSentencia) {
			
			sentenciaEncriptada += reglaAUtilizar.aplicaPara(letraActual);
		}
		
		return sentenciaEncriptada;
	}
}
