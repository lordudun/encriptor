package com.iexpertos.encriptor;

public class Encriptor {
	
	private Regla reglaDeEnciptacion;
	
	public Encriptor(Regla reglaDeEnciptacion) {
		
		this.reglaDeEnciptacion = reglaDeEnciptacion;
	}
	
	public String encripta(Sentencia sentenciaAEncriptar) {
		
		return sentenciaAEncriptar.convierte(reglaDeEnciptacion);
	}
}
