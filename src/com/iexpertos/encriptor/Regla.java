package com.iexpertos.encriptor;

public interface Regla {

	public String aplicaPara(char estaLetra);
}
